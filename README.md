# centenarianmicrobiome

Placeholder for in-house scripts used in Centenarian microbiome collaboration with Kenya Honda lab at Keio University for quality control on BWA read mapping and summarizing gene-level read counts.